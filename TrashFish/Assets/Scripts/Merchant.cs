﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Merchant : MonoBehaviour
{
    [SerializeField] Text RodText;
    [SerializeField] Text MerchantText;
    public static bool z;
    public static bool mAccepted = false;

    public GameObject Object1;
    public GameObject Player;
    public float Distance_;
    // Start is called before the first frame update
    void Start()
    {
        MerchantText.gameObject.SetActive(false);
        RodText.gameObject.SetActive(false);
    }
    void OnMouseDown()
    {
        bool z = gameObject.CompareTag("Merchant");
        if (Distance_ < 3 && z)
        {
            mAccepted = true;
            MerchantText.gameObject.SetActive(true);
            Object.Destroy(MerchantText, 4.0f);
            RodText.gameObject.SetActive(true);
            Object.Destroy(RodText, 7.0f);

        }
    }
    // Update is called once per frame
    void Update()
    {
        Distance_ = Vector3.Distance(Object1.transform.position, Player.transform.position);

   
    }
}
