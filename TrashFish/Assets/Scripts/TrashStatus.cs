﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashStatus : MonoBehaviour
{
    public WeightCounter other;
    public SpriteRenderer spriteRenderer;
    public Sprite Status1;
    public Sprite Status2;

    public static int trashCount;

    // Start is called before the first frame update
    void Start()
    {
        trashCount = 0;
    }
    void OnMouseDown()
    {
        if (gameObject.CompareTag("TC") && WeightCounter.weightCount >= 50)
        {
            Debug.Log("hi");
            WeightCounter.weightCount = 0;
            spriteRenderer.sprite = Status1;
        }
        else if (gameObject.CompareTag("TC") && WeightCounter.weightCount >= 75)
        {
            WeightCounter.weightCount = 0;
            spriteRenderer.sprite = Status2;
        }
    }
    void TrashWeight()
    {
        if (WeightCounter.weightCount > 50)
        {
            spriteRenderer.sprite = Status1;
        }
    }


    // Update is called once per frame
    void Update()
    {
        other.SetWeight();
     
    }
}
