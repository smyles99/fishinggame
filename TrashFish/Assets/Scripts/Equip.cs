﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equip : MonoBehaviour
{
    [SerializeField] public GameObject item1;

    public static bool showItem1;


    // Start is called before the first frame update
    void Start()
    {
        showItem1 = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (showItem1 == false)
        {
            item1.SetActive(false);
        }
        if (showItem1 == true)
        {
            item1.SetActive(true);
            
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && showItem1 == false && Merchant.mAccepted)
        {
            showItem1 = true;
        }


        if (Input.GetKeyDown(KeyCode.R))
        {
            showItem1 = false;

        }
        
    }
}
