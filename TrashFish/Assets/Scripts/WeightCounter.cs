﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeightCounter : MonoBehaviour
{
    public TrashStatus other;
    //[SerializeField] Text weightText;
    public Text weightText;
    public static int weightCount;

    public GameObject Object1;
    public GameObject Player1;
    public float Distance_;
    // Start is called before the first frame update
    void Start()
    {
        weightCount = 0;
        SetWeight();

    }
    public void SetWeight()
    {
        weightText.text = "Weight: " + weightCount.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        Distance_ = Vector3.Distance(Object1.transform.position, Player1.transform.position);

        if (weightCount > 50)
        {
            Player.runSpeed = 3.0f;
            Debug.Log("Incumbered");
        }
    }
     void OnMouseDown()
    {
        bool c = gameObject.CompareTag("Can");
        bool v = gameObject.CompareTag("Tire");
        bool y = gameObject.CompareTag("Bottle");
        if ((v) && Equip.showItem1 && Distance_ < 5)
            
        {
            weightCount += 15;
            SetWeight();
            gameObject.SetActive(false);
            //print("hoopla");
        }
        else if ((c) && Equip.showItem1 && Distance_ < 5)
        {
            weightCount += 2;
            SetWeight();
            gameObject.SetActive(false);
            //print("hoopla");
        }
        else if ((y) && Equip.showItem1 && Distance_ < 5)
        {
            weightCount += 5;
            SetWeight();
            gameObject.SetActive(false);
        }
    }

}
